﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    static class Expressao
    {

        static Random rnd = new Random();

        public static int contaNodos(Expression exp)
        {
            var cond = exp as ConditionalExpression;
            if (cond != null)
                return 1 + contaNodos(cond.IfTrue) + contaNodos(cond.IfFalse);
            var lambda = exp as LambdaExpression;
            if (lambda != null)
                return contaNodos(lambda.Body);
            return 0;
        }


        static Decisao sorteiaDecisao()
        {
            switch (rnd.Next(3))
            {
                case 0:
                    return Decisao.Comprar;
                case 1:
                    return Decisao.Vender;
                default:
                    return Decisao.Esperar;
            }
        }


        //Expression<Func<IAvaliadorExpressao, Decisao>> CrossOverUniforme(Expression<Func<IAvaliadorExpressao, Decisao>> A, Expression<Func<IAvaliadorExpressao, Decisao>> B)
        //{
        //    var parametros = A.Parameters;
        //    var parametroPos = parametros.First();
        //    var novobody = CrossOver(A.Body, B.Body, parametroPos);
        //    var retorno = (Expression<Func<IAvaliadorExpressao, Decisao>>)Expression.Lambda(novobody, parametroPos);
        //    return retorno;
        //}

        static List<ConditionalExpression> ListaNodos(Expression exp)
        {
            var lista = new List<ConditionalExpression>();
            var fila = new Queue<ConditionalExpression>();
            if (exp is LambdaExpression)
            {
                exp = ((LambdaExpression)exp).Body;
            }
            var cond = exp as ConditionalExpression;
            if (cond != null)
                fila.Enqueue(cond);

            while (fila.Any())
            {
                cond = fila.Dequeue();
                lista.Add(cond);
                var condchild = cond.IfTrue as ConditionalExpression;
                if (condchild != null)
                    fila.Enqueue(condchild);
                condchild = cond.IfFalse as ConditionalExpression;
                if (condchild != null)
                    fila.Enqueue(condchild);
            }

            return lista;
        }


        static Expression AjustaParametro(Expression exp, Expression parametro)
        {
            var call = exp as MethodCallExpression;
            if (call != null)
                return Expression.Call(parametro, call.Method, call.Arguments);

            var cond = exp as ConditionalExpression;
            if (cond != null)
                return Expression.Condition(AjustaParametro(cond.Test, parametro), AjustaParametro(cond.IfTrue, parametro), AjustaParametro(cond.IfFalse, parametro));
            return exp;
        }


        static Expression Clone(Expression exp, Expression original, Expression substituto)
        {
            if (exp == original)
                return substituto;
            if (exp is BinaryExpression)
            {
                var binary = (BinaryExpression)exp;
                return Expression.MakeBinary(binary.NodeType, Clone(binary.Left, original, substituto), Clone(binary.Right, original, substituto));
            }
            var cond = exp as ConditionalExpression;
            if (cond != null)
            {
                return Expression.Condition(Clone(cond.Test, original, substituto), Clone(cond.IfTrue, original, substituto), Clone(cond.IfFalse, original, substituto));
            }
            return exp;
        }

        public static Expression<Func<IAvaliadorExpressao, Decisao>> CrossOverUmPonto(Expression<Func<IAvaliadorExpressao, Decisao>> A, Expression<Func<IAvaliadorExpressao, Decisao>> B)
        {
            var nodosa = ListaNodos(A);
            var nodosb = ListaNodos(B);
            var paramPos = A.Parameters.First();
            var nodoOriginal = nodosa[rnd.Next(nodosa.Count())];
            var novoNodo = AjustaParametro(nodosb[rnd.Next(nodosb.Count())], paramPos);

            if (rnd.Next(2) == 0)
                novoNodo = Expression.Condition(nodoOriginal.Test, novoNodo, nodoOriginal.IfFalse);
            else
                novoNodo = Expression.Condition(nodoOriginal.Test, nodoOriginal.IfTrue, novoNodo);

            var corpo = Clone(A.Body, nodoOriginal, novoNodo);
            return (Expression<Func<IAvaliadorExpressao, Decisao>>)Expression.Lambda(corpo, paramPos);
        }

        static ConditionalExpression PodaNodo(ConditionalExpression nodo, Expression posicao)
        {
            switch (rnd.Next(4))
            {
                case 0: // poda para lado verdadeiro
                    if (nodo.IfTrue is ConditionalExpression)
                        return (ConditionalExpression)nodo.IfTrue;
                    else
                        return Expression.Condition(nodo.Test, Expression.Constant(sorteiaDecisao()), nodo.IfFalse);
                case 1: // poda para lado falso (se não der, muda resultado do lado falso)
                    if (nodo.IfFalse is ConditionalExpression)
                        return (ConditionalExpression)nodo.IfFalse;
                    else
                        return Expression.Condition(nodo.Test, nodo.IfTrue, Expression.Constant(sorteiaDecisao()));

                case 2: // poda para lado verdadeiro
                    return Expression.Condition(nodo.Test, Poda(nodo.IfTrue), nodo.IfFalse);
                case 3:
                default: // poda para lado falso
                    return Expression.Condition(nodo.Test, nodo.IfTrue, Poda(nodo.IfFalse));
            }
        }



        static Expression Poda(Expression exp)
        {
            ConditionalExpression cond = exp as ConditionalExpression;
            if (cond != null)
            {
                if (rnd.Next(2) == 0)
                    return cond.IfTrue;
                else
                    return cond.IfFalse;
            }
            return Expression.Constant(sorteiaDecisao());
        }

        static int sorteiaValorParametro(DadosParametro par)
        {
            int min = par.min / par.passo;
            int max = (par.max / par.passo) + 1;
            int valor = rnd.Next(min, max) * par.passo;
            return valor;
        }




        static Expression ModificaTeste(Expression original, Expression posicao)
        {
            if (rnd.Next(10) < 3)
                return sorteiaTeste(posicao);
            var cm = original as MethodCallExpression;
            var metodo = Metodos.LeMetodos().Where(x => x.metodo == cm.Method).First();
            int p = rnd.Next(metodo.dadosparametros.Length);
            var arguments = cm.Arguments.ToArray();
            int valororiginal = (int)(arguments[p] as ConstantExpression).Value;
            int novovalor;
            if (rnd.Next(2) == 0)
                novovalor = valororiginal - metodo.dadosparametros[p].passo;
            else
                novovalor = valororiginal + metodo.dadosparametros[p].passo;
            if (novovalor < metodo.dadosparametros[p].min)
                novovalor = metodo.dadosparametros[p].min;
            if (novovalor > metodo.dadosparametros[p].max)
                novovalor = metodo.dadosparametros[p].max;
            arguments[p] = Expression.Constant(novovalor);
            return Expression.Call(posicao, cm.Method, arguments);
        }


        static Expression sorteiaTeste(Expression posicao)
        {
            var metodos = Metodos.LeMetodos();
            var metodo = metodos[rnd.Next(metodos.Length)];
            Expression[] valores = metodo.dadosparametros
                .Select(par => Expression.Constant(sorteiaValorParametro(par))).ToArray();
            return Expression.Call(posicao, metodo.metodo, valores);
        }

        static ConditionalExpression CriaNodoCondicional(Expression posicao, Expression original)
        {
            var test = sorteiaTeste(posicao);
            if (original != null)
            {
                switch (rnd.Next(3))
                {
                    case 0:
                        return Expression.Condition(test, original, Expression.Constant(sorteiaDecisao()));
                    case 1:
                        return Expression.Condition(test, Expression.Constant(sorteiaDecisao()), original);
                }
            }
            var iftrue = sorteiaDecisao();
            var iffalse = iftrue;
            while (iffalse == iftrue)
                iffalse = sorteiaDecisao();
            return Expression.Condition(test, Expression.Constant(iftrue), Expression.Constant(iffalse));
        }


        static ConditionalExpression ModificaNodo(ConditionalExpression nodo, Expression posicao)
        {
            switch (rnd.Next(3))
            {
                case 0: // insere novo nodo acima
                    return CriaNodoCondicional(posicao, nodo);
                case 1:
                    return PodaNodo(nodo, posicao);
                case 2:
                default:
                    return Expression.Condition(ModificaTeste(nodo.Test, posicao), nodo.IfTrue, nodo.IfFalse);
            }
        }

        public static Expression<Func<IAvaliadorExpressao, Decisao>> ModificaExpressao(Expression<Func<IAvaliadorExpressao, Decisao>> expressao)
        {
            Expression corpo = ((LambdaExpression)expressao).Body;
            var parametros = ((LambdaExpression)expressao).Parameters;
            var lista = ListaNodos(corpo);
            var nodoOriginal = lista.ElementAt(rnd.Next(lista.Count()));
            var nodoModificado = ModificaNodo(nodoOriginal, parametros[0]);
            corpo = Clone(corpo, nodoOriginal, nodoModificado);
            var retorno = (Expression<Func<IAvaliadorExpressao, Decisao>>)Expression.Lambda(corpo, parametros);
            return retorno;
        }

        static ConditionalExpression CriaSubArvore(Expression parametro, int altura)
        {
            Expression verdadeiro;
            Expression falso;
            if (altura > 1)
            {
                verdadeiro = CriaSubArvore(parametro, altura - 1);
                falso = CriaSubArvore(parametro, altura - 1);
            }
            else
            {
                verdadeiro = Expression.Constant(sorteiaDecisao());
                falso = Expression.Constant(sorteiaDecisao());
            }
            return Expression.Condition(sorteiaTeste(parametro), verdadeiro, falso);
        }

        public static Expression<Func<IAvaliadorExpressao, Decisao>> CriaExpressao()
        {
            var parametro = Expression.Parameter(typeof(IAvaliadorExpressao), "pos");
            var cond = CriaSubArvore(parametro, GeradorExpressoes.profundidadeInicial);
            var expressao = (Expression<Func<IAvaliadorExpressao, Decisao>>)Expression.Lambda(cond, parametro);
            return expressao;
        }
    }
}
