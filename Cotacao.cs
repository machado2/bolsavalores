﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    public class Cotacao
    {
        public DateTime Data;
        public double Open;
        public double High;
        public double Low;
        public double Close;
        public double Volume;
        public double AdjClose;
        public bool Anomalia;

        public class ComparadorData : IComparer<Cotacao>
        {
            public int Compare(Cotacao x, Cotacao y)
            {
                return x.Data.CompareTo(y.Data);
            }
        }

        public static Cotacao[] LerCSV(string nome)
        {
            List<Cotacao> lista = new List<Cotacao>();
            foreach (string linha in File.ReadAllLines(nome))
            {
                string[] partes = linha.Split(',');
                if (partes.Length != 7)
                    continue;
                DateTime data;
                if (!DateTime.TryParseExact(partes[0], "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out data))
                    continue;
                try
                {
                    var item = new Cotacao();
                    item.Data = data;
                    item.Open = double.Parse(partes[1], CultureInfo.InvariantCulture);
                    item.High = double.Parse(partes[2], CultureInfo.InvariantCulture);
                    item.Low = double.Parse(partes[3], CultureInfo.InvariantCulture);
                    item.Close = double.Parse(partes[4], CultureInfo.InvariantCulture);
                    item.Volume = double.Parse(partes[5], CultureInfo.InvariantCulture);
                    item.AdjClose = double.Parse(partes[6], CultureInfo.InvariantCulture);
                    item.Anomalia = false;
                    if (item.Close < 0.10)
                        continue; // evitar cotações zeradas ou problemáticas
                    lista.Add(item);
                }
                catch
                {
                    continue;
                }
            }
            lista.Sort(new Cotacao.ComparadorData());
            var ret = lista.ToArray();
            for (int i = 0; i < (ret.Length - 1); i++)
            {
                if (i < 365)
                    ret[i].Anomalia = true;
                if (Math.Abs(ret[i].Close - ret[i + 1].Close) > ret[i].Close / 5)
                {
                    int p1 = i - 90;
                    if (p1 < 0)
                        p1 = 0;
                    int p2 = i + 90;
                    if (p2 > ret.Length)
                        p2 = ret.Length;
                    for (int j = p1; j < p2; j++)
                        ret[j].Anomalia = true;
                }
            }
            return ret;
        }
    }
}
