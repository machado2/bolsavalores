﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    class Simulacao
    {
        Acao[] Acoes;

        public Simulacao(Acao[] acoes)
        {
            this.Acoes = acoes;
            //var cotacoes = this.Acoes.SelectMany(acao => acao.historico.Select((cotacao, posicao) =>
            //    new PosicaoAcao(posicao, acao)));
            //agrupadoData = cotacoes
            //    .GroupBy(x => x.Data)
            //    .ToDictionary(x => x.Key, x => x.ToArray());
        }

        class CotacaoDia
        {
            public int posicao { get; set; }
            public Acao acao { get; set; }
            public DateTime data { get; set; }
        }

        public void Simular(IEnumerable<Investidor> investidores, DateTime dtIni, DateTime dtFim, bool teste = false)
        {
            // 2002 a bovespa caiu
            // 2004 a bovespa subiu
            //DateTime dtIni = new DateTime(2002, 01, 01);
            //DateTime dtFim = dtIni.AddYears(2);
            var rnd = new Random();
            foreach (var investidor in investidores)
                investidor.Resetar();

            /*var acoesSelecionadas = this.Acoes.Where(x => x.ticker == "GGBR4.SA").ToList();
            if (teste) acoesSelecionadas = this.Acoes.ToList();
            */
            var acoesSelecionadas = this.Acoes.ToList();
                

            for (DateTime data = dtIni; data < dtFim; data = data.AddDays(1))
            {
                List<PosicaoAcao> posicoes = new List<PosicaoAcao>();
                foreach (var acao in acoesSelecionadas)
                {
                    var p = acao.PosicaoData(data);
                    if (p != null)
                        posicoes.Add(p);
                }
                if (!posicoes.Any())
                    continue;

                foreach (var investidor in investidores)
                {
                    investidor.RealizaOperacoes(posicoes, data, teste);
                }
            }
        }

        public void Simular(Investidor investidor, DateTime dtIni, DateTime dtFim)
        {
            Simular(new Investidor[] { investidor }, dtIni, dtFim, true);
        }
    }
}
