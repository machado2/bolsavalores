﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace bolsa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Simulacao sim = new Simulacao(Acao.LerHistoricosPasta("c:\\fabio\\bolsa\\dados"));

        void ProcessoBackGround(Button btn, Label lbl, Func<string> acao)
        {
            Cursor = Cursors.WaitCursor;
            btn.Enabled = false;
            Task.Factory.StartNew((Action)delegate
            {
                string strResult = "";
                try
                {
                    strResult = acao();
                }
                catch (Exception erro)
                {
                    strResult = erro.ToString();
                }
                finally
                {
                    Invoke((Action)delegate
                    {
                        lbl.Text = strResult;
                        btn.Enabled = true;
                        Cursor = Cursors.Default;
                    });
                }
            });
        }

        

        volatile bool rodando = false;

        static Random rnd = new Random();

        void processoGA()
        {
            GeradorExpressoes ger;
            // 2002 a bovespa caiu
            // 2004 a bovespa subiu
            //DateTime dtIni = new DateTime(2002, 01, 01);
            //DateTime dtFim = dtIni.AddYears(2);


            DateTime dtIniTeste = new DateTime(2009, 1, 1);
            DateTime dtFimTeste = dtIniTeste.AddYears(1);

            if (rodando)
                return;
            rodando = true;
            ger = new GeradorExpressoes(sim);
            string arqPopulacao = "c:\\fabio\\bolsa\\populacao.xml";
            if (File.Exists(arqPopulacao))
                ger.Deserializa(arqPopulacao);
            else
                ger.Reseta();

            Cursor = Cursors.WaitCursor;
            btnProcessaLoop.Enabled = false;



            Task.Factory.StartNew((Action)delegate
            {
                //ger.Reseta();
                //ger.Serializa("c:\\fabio\\bolsa\\populacao.xml");
                //return;
                int geracao = 1;
                DateTime ultimaGravacao = DateTime.Now;
                string ultimoTeste = "Ainda não testado";
                while (rodando)
                {
                    var strResult = new StringBuilder();
                    try
                    {
                        strResult.Clear();
                        strResult.AppendLine("Geração: " + geracao.ToString());
                        DateTime dtIni = new DateTime(2002, 1, 1);
                        //dtIni = dtIni.AddDays(rnd.Next(365 * 3));
                        DateTime dtFim =
                            dtIni.AddYears(3);
                        //dtIni.AddMonths(1);

                        var exp = ger.EncontraMelhorExpressao(1, dtIni, dtFim);

                        strResult.AppendLine();
                        strResult.AppendLine(ultimoTeste);
                        strResult.AppendLine();

                        strResult.AppendLine("pontuacao " + exp.Fitness.ToString("0"));
                        strResult.AppendLine("função: " + exp.ToString());
                        strResult.AppendLine();

                        var expteste = ger.MaisComum();
                        strResult.AppendLine();
                        strResult.AppendLine("função mais comum: ");
                        strResult.AppendLine(expteste.ToString());
                        strResult.AppendLine();

                        if ((DateTime.Now - ultimaGravacao).TotalMinutes > 1)
                        {
                            ger.EscreveDebug("c:\\fabio\\bolsa\\debug.txt");
                            ger.Serializa("c:\\fabio\\bolsa\\populacao.xml");
                            ger.SerializaMaisComum("c:\\fabio\\bolsa\\melhor.xml");
                            ultimaGravacao = DateTime.Now;
                            strResult.AppendLine("gravado");
                            sim.Simular(expteste, dtIniTeste, dtFimTeste);
                            ultimoTeste = "Teste: " + expteste.Fitness.ToString("0.00")
                                + Environment.NewLine + "Função testada: " + expteste.ToString();
                        }
                        else
                        {
                            //double resultTeste = sim.Simular(5000, exp.comportamento, dtIniTeste, dtFimTeste);
                            //ultimoTeste = "Teste: " + resultTeste.ToString("0.00")
                            //    + Environment.NewLine + "Função testada: " + exp.comportamento.ToString();

                        }
                        geracao++;
                    }
                    catch (Exception erro)
                    {
                        strResult.AppendLine(erro.ToString());
                    }
                    finally
                    {
                        try
                        {
                            if (!this.IsDisposed)
                            {
                                Invoke((Action)delegate
                                {
                                    txtResultado.Text = strResult.ToString();
                                });
                            }
                        }
                        catch { }
                    }
                }
            });

        }

        private void btnProcessaLoop_Click(object sender, EventArgs e)
        {
            DateTime dtIniTeste = new DateTime(2009, 1, 1);
            DateTime dtFimTeste = dtIniTeste.AddYears(1);

            var la = new LateAcceptance();

            string nomeMelhor = "c:\\fabio\\bolsa\\melhor.xml";
            string nomeLate = "c:\\fabio\\bolsa\\lateaccept.xml";

            if (File.Exists(nomeLate))
            {
                la.Deserializar(XElement.Load(nomeLate));
            }

            if (rodando)
                return;
            rodando = true;
            
            Cursor = Cursors.WaitCursor;
            btnProcessaLoop.Enabled = false;
            Task.Factory.StartNew((Action) delegate {
                int geracao = 1;
                DateTime ultimaGravacao = DateTime.Now.AddDays(-1);
                string ultimoTeste = "Ainda não testado";
                while (rodando)
                {
                    var strResult = new StringBuilder();
                    try
                    {
                        strResult.Clear();
                        strResult.AppendLine("Geração: " + geracao.ToString());
                        for (int i = 0; i < 20; i++)
                            la.Iteracao();
                        var exp = la.atual;
                        
                        strResult.AppendLine();
                        strResult.AppendLine(ultimoTeste);
                        strResult.AppendLine();

                        strResult.AppendLine("pontuacao " + exp.Fitness.ToString("0.00000"));
                        strResult.AppendLine("função: " + exp.ToString());
                        strResult.AppendLine();

                        var expteste = la.melhor;
                        strResult.AppendLine();
                        strResult.AppendLine("função melhor: ");
                        strResult.AppendLine(expteste.Fitness.ToString("0.00000"));
                        strResult.AppendLine(expteste.ToString());
                        strResult.AppendLine();
                        
                        if ((DateTime.Now - ultimaGravacao).TotalMinutes > 1)
                        {
                            //la.atual = la.melhor;
                            expteste.serializar().Save(nomeMelhor);
                            la.Serializar().Save(nomeLate);
                            ultimaGravacao = DateTime.Now;
                            strResult.AppendLine("gravado");
                            ultimoTeste = "Teste: " + expteste.Validar()
                                + Environment.NewLine + "Função testada: " + expteste.ToString();
                        }
                        else
                        {
                            //double resultTeste = sim.Simular(5000, exp.comportamento, dtIniTeste, dtFimTeste);
                            //ultimoTeste = "Teste: " + resultTeste.ToString("0.00")
                            //    + Environment.NewLine + "Função testada: " + exp.comportamento.ToString();

                        }
                        geracao++;
                    }
                    catch (Exception erro)
                    {
                        strResult.AppendLine(erro.ToString());
                    }
                    finally
                    {
                        try
                        {
                            if (!this.IsDisposed)
                            {
                                Invoke((Action)delegate
                                {
                                    txtResultado.Text = strResult.ToString();
                                });
                            }
                        }
                        catch { }
                    }
                }
            });
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            rodando = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            btnProcessaLoop_Click(sender, e);
        }
    }
}
