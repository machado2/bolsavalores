﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    [AttributeUsage(AttributeTargets.Parameter, Inherited = false, AllowMultiple = false)]
    sealed class FaixaAttribute : Attribute
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public int Passo { get; set; }
        public FaixaAttribute(int min, int max, int passo)
        {
            this.Min = min;
            this.Max = max;
            this.Passo = passo;
        }
    }

    [AttributeUsage(AttributeTargets.Method, Inherited=false, AllowMultiple=false)]
    sealed class IgnorarAttribute : Attribute
    {

    }

}
