﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace bolsa
{
    class LateAcceptance
    {
        const int atraso = 400;

        public Investidor melhor;
        public Investidor atual;
        public double scoreatual = 0;
        public double scoremelhor = 0;
        double[] ultimaspontuacoes;
        int pultima = 0;

        public LateAcceptance()
        {
            ultimaspontuacoes = new double[atraso];
            atual = Investidor.CriaInvestidor();
            scoreatual = atual.Fitness;
            melhor = atual;
            scoremelhor = scoreatual;
            for (int i = 0; i < ultimaspontuacoes.Length; i++)
                ultimaspontuacoes[i] = scoreatual;
        }

        public void Iteracao()
        {
            Investidor tentativa = atual.Mutation();
            double score = tentativa.Fitness;
            if (score >= scoreatual || score >= ultimaspontuacoes[pultima])
            {
                atual = tentativa;
                scoreatual = score;
                if (score > scoremelhor)
                {
                    scoremelhor = score;
                    melhor = atual;
                }
            }
            ultimaspontuacoes[pultima] = scoreatual;
            pultima++;
            if (pultima >= ultimaspontuacoes.Length)
                pultima = 0;
        }

        public XElement Serializar()
        {
            return new XElement("lateacceptance",
                new XElement("melhor", melhor.serializar()),
                new XElement("atual", atual.serializar()),
                new XElement("ultimaspontuacoes", string.Join(";", ultimaspontuacoes.Select(x => x.ToString()))),
                new XElement("pultima", this.pultima));
        }

        public void Deserializar(XElement xml)
        {
            melhor = Investidor.Deserializar(xml.Element("melhor").Element("Investidor"));
            atual = Investidor.Deserializar(xml.Element("atual").Element("Investidor"));
            pultima = int.Parse(xml.Element("pultima").Value);
            ultimaspontuacoes = xml.Element("ultimaspontuacoes").Value.Split(';').Select(x => double.Parse(x)).ToArray();
            scoreatual = atual.Fitness;
            scoremelhor = melhor.Fitness;
        }

    }
}
