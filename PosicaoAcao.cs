﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{

    public class PosicaoAcao
    {
        int posicao;
        public Acao acao;

        public PosicaoAcao(int posicao, Acao acao)
        {
            this.posicao = posicao;
            this.acao = acao;
        }

        public DateTime Data
        {
            get
            {
                return this.acao.historico[this.posicao].Data;
            }
        }

        public double TaxaVariacao
        {
            get
            {
                if (posicao <= 0)
                    return 0;
                double anterior = acao.historico[posicao - 1].Close;
                double atual = acao.historico[posicao].Close;
                if (anterior == 0)
                    return 0;
                return (atual - anterior) / anterior;
            }
        }

        public bool ExisteAnomalia
        {
            get
            {
                return acao.historico[this.posicao].Anomalia;
            }
        }

        public Cotacao Cotacao
        {
            get
            {
                return acao.historico[posicao];
            }
        }

        public double ValorAtual()
        {
            return acao.historico[posicao].Close;
        }

        class Analise 
        {
            Cotacao[] cotacoes;
            PosicaoAcao pa;
            public Analise(int dias, PosicaoAcao pa)
            {
                this.pa = pa;
                int p1 = pa.posicao - dias;
                if (p1 < 0) p1 = 0;
                // pos = 10
                // 1 dia
                // p1 = 10 - 1 = 9
                // count = 10 - 9  = 1
                // selecionados = 9
                cotacoes = new ArraySegment<Cotacao>(pa.acao.historico, p1, pa.posicao - p1).ToArray();
            }

            double? media;
            public double Media()
            {
                if (!media.HasValue)
                    media = cotacoes.Average(x => x.Close);
                return media.Value;
            }

            Dictionary<int, double> percentis = new Dictionary<int, double>();
            public double percentil(int perc)
            {
                double v;
                if (percentis.TryGetValue(perc, out v))
                    return v;
                int posicao = perc * (cotacoes.Length - 1) / 100;
                v = cotacoes.OrderBy(x => x.Close).ElementAt(posicao).Close;
                percentis[perc] = v;
                return v;
            }

            Dictionary<int, int> oscil = new Dictionary<int, int>();

            public int numOscilacoes(int varperc)
            {
                int a;
                if (oscil.TryGetValue(varperc, out a))
                    return a;

                double percbaixo = percentil(50 - varperc);
                double percalto = percentil(50 + varperc);
                bool? acima = null;
                int oscilacoes = 0;
                for (int i = 0; i < cotacoes.Length; i++)
                {
                    var valor = cotacoes[i].Close;
                    if (acima != false && valor < percbaixo)
                    {
                        if (acima != null)
                            oscilacoes++;
                        acima = false;
                    }
                    else if (acima != true && valor > percalto)
                    {
                        if (acima != null)
                            oscilacoes++;
                        acima = true;
                    }
                }
                oscil[varperc] = oscilacoes;
                return oscilacoes;
            }
            int? amplitude;
            public int Amplitude
            {
                get
                {
                    if (amplitude == null)
                    {
                        double min = cotacoes[0].Close;
                        double max = min;
                        foreach (var cot in cotacoes)
                        {
                            if (cot.Close < min)
                                min = cot.Close;
                            else if (cot.Close > max)
                                max = cot.Close;
                        }
                        double delta = max - min;
                        if (min == 0) return 100;
                        amplitude = (int)(delta * 100 / min);
                    }
                    return amplitude.Value;
                }
            }

            double? vmmex;
            public double mmex()
            {
                if (vmmex == null)
                {

                    double v = cotacoes[0].Close;
                    double k = 2 / (cotacoes.Length + 1);
                    for (int i = 1; i < cotacoes.Length; i++)
                    {
                        v = v + k * (cotacoes[i].Close - v);
                    }
                    vmmex = v;
                }
                return vmmex.Value;
            }
            
        }

        Dictionary<int, Analise> dicAnalise = new Dictionary<int, Analise>();

        Analise Fatia(int dias)
        {
            Analise a;
            if (dicAnalise.TryGetValue(dias, out a))
                return a;
                
            a = new Analise(dias, this);
            dicAnalise[dias] = a;
            return a;
        }

        public double Media(int dias)
        {
            return Fatia(dias).Media();
        }

        public double MMEX(int dias)
        {
            return Fatia(dias).mmex();
        }

        public bool MenorQuePercentil(int perc, int dias){
            return ValorAtual() < Fatia(dias).percentil(perc);
        }

        public bool MaiorQuePercentil(int perc, int dias)
        {
            return ValorAtual() > Fatia(dias).percentil(perc);
        }



        public bool MenosQueXOscilacoes(int dias, int varperc, int x) {
            return Fatia(dias).numOscilacoes(varperc) <  x;
        }

        public bool MaiorQueXOscilacoes(int dias, int varperc, int x)
        {
            return Fatia(dias).numOscilacoes(varperc) > x;
        }

        public bool AmplitudeMenorQuePerc(int dias, int perc)
        {
            return Fatia(dias).Amplitude < (perc);
        }

        public bool AmplitudeMaiorQuePerc(int dias, int perc)
        {
            return Fatia(dias).Amplitude < perc;
        }

    }
}
