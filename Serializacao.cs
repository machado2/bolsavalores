﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;
using System.Xml.Linq;
namespace bolsa
{
    class Serializacao
    {
        public static Investidor DeserializarMelhor(string nomeArq)
        {
            var xml = XDocument.Load(nomeArq);
            var raiz = xml.Root;
            var exp = Investidor.Deserializar(raiz);
            return exp;
        }

        static XElement Serializa(ConditionalExpression exp)
        {
            return new XElement("ConditionalExpression",
                new XElement("test", Serializa((MethodCallExpression) exp.Test)),
                new XElement("iftrue", Serializa(exp.IfTrue)),
                new XElement("iffalse", Serializa(exp.IfFalse)));
        }

        static string Serializa(MethodCallExpression exp)
        {
            return exp.Method.Name + "," + string.Join(",", exp.Arguments.Select(x => ((ConstantExpression)x).Value.ToString()));
        }

        public static ConditionalExpression DeserializaConditionalExp(XElement exp, Expression posicao)
        {
            string[] strTest = exp.Element("test").Value.Split(',');
            var mets = Metodos.LeMetodos();

            var mi = mets.Where(x => x.metodo.Name == strTest[0]).First().metodo;
            var arguments = strTest.Skip(1).Select(s => Expression.Constant(int.Parse(s)));
            var test = Expression.Call(posicao, mi, arguments);

            var iftrue = Deserializa(exp.Element("iftrue").Elements().First(), posicao);
            var iffalse = Deserializa(exp.Elements("iffalse").Elements().First(), posicao);
            return Expression.Condition(test, iftrue, iffalse);
        }

        public static Expression Deserializa(XElement exp, Expression posicao)
        {
            if (exp.Name.LocalName == "ConditionalExpression")
                return DeserializaConditionalExp(exp, posicao);
            if (exp.Name.LocalName == "decisao")
            {
                var decisao = Enum.Parse(typeof(Decisao), exp.Value);
                return Expression.Constant(decisao);
            }
            throw new NotImplementedException();
        }

        public static Expression<Func<IAvaliadorExpressao, Decisao>> DeserializaExpressao(XElement exp)
        {
            var param = Expression.Parameter(typeof(IAvaliadorExpressao), "pos");
            var body = Deserializa(exp, param);
            var result = Expression.Lambda(body, param);
            return (Expression<Func<IAvaliadorExpressao, Decisao>>)result;
        }

        public static XElement Serializa(Expression exp)
        {
            if (exp is ConditionalExpression)
                return Serializa((ConditionalExpression)exp);
            if (exp is ConstantExpression)
                return new XElement("decisao", ((ConstantExpression)exp).Value.ToString());
            if (exp is LambdaExpression)
                return Serializa(((LambdaExpression)exp).Body);
            throw new NotImplementedException();
        }
    }
}
