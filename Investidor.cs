﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace bolsa
{
    class Investidor : ISolucao<Investidor>
    {
        Carteira carteira;
        public Func<IAvaliadorExpressao, Decisao> compilado;
        public Expression<Func<IAvaliadorExpressao, Decisao>> comportamento;
        public double TaxaStop;
        public double TaxaVenda;
        public double TaxaCompra;
        private double Pontuacao;

        public Investidor(Expression<Func<IAvaliadorExpressao, Decisao>> comportamento, double taxaStop, double taxaVenda, double taxaCompra)

        {
            carteira = new Carteira();
            this.comportamento = comportamento;
            this.compilado = comportamento.Compile();
            this.TaxaStop = taxaStop;
            this.TaxaVenda = taxaVenda;
            this.TaxaCompra = taxaCompra;
        }

        public Decisao calculaDecisao(PosicaoAcao posicao)
        {
            if (posicao.ExisteAnomalia)
                return Decisao.Vender;
            else
                return compilado(new AvaliadorExpressao(posicao, this));
        }

        static Random rnd = new Random();

        public void RealizaOperacoes(IEnumerable<PosicaoAcao> posicoes, DateTime data, bool modoTeste)
        {
            carteira.ExecutarOrdens(posicoes, TaxaStop, TaxaCompra, TaxaVenda, modoTeste);
#if DEBUG
            if (ValorTotal() > 100000)
            {
            }
#endif

            List<PosicaoAcao> comprar = new List<PosicaoAcao>();
            foreach (var posicao in posicoes)
            {
                switch (calculaDecisao(posicao))
                {
                    case Decisao.Comprar:
                        carteira.InserirOrdemCompra(posicao.acao, posicao.ValorAtual(), 5000);
                        break;
                    case Decisao.Vender:
                        carteira.InserirOrdemVenda(posicao.acao);
                        break;
                }
            }
        }

        public double ValorTotal()
        {
            return carteira.ValorTotal();
        }

        public double? CustoMedio(Acao acao)
        {
            Carteira.Item item;
            if (carteira.Acoes.TryGetValue(acao, out item)) {
                return item.PrecoMedio;
            } else
                return null;
        }

        public int? DiasDesdeCompra(Acao acao, DateTime data)
        {
            Carteira.Item item;
            if (carteira.Acoes.TryGetValue(acao, out item))
            {
                return (int) (data - item.DataCompra).TotalDays;
            }
            else
                return null;
        }

        public XElement serializar()
        {
            var serial = new Serializacao();
            return new XElement("Investidor",
                new XElement("TaxaStop", TaxaStop),
                new XElement("TaxaCompra", TaxaCompra),
                new XElement("TaxaVenda", TaxaVenda),
                Serializacao.Serializa(comportamento));
        }

        public static Investidor Deserializar(XElement xml)
        {
            double tstop = double.Parse(xml.Element("TaxaStop").Value, CultureInfo.InvariantCulture);
            double tcompra = double.Parse(xml.Element("TaxaCompra").Value, CultureInfo.InvariantCulture);
            double tvenda = double.Parse(xml.Element("TaxaVenda").Value, CultureInfo.InvariantCulture);
            var exp = Serializacao.DeserializaExpressao(xml.Element("ConditionalExpression"));
            return new Investidor(exp, tstop, tvenda, tcompra);
        }


        public int contaNodos()
        {
            return Expressao.contaNodos(comportamento);
        }

        private void CalculaPontuacao()
        {
            var dinheiro = carteira.ValorTotal();
            while (comportamento.CanReduce)
                comportamento = (Expression<Func<IAvaliadorExpressao, Decisao>>)comportamento.Reduce();
            int numNodos = contaNodos();
            Pontuacao = dinheiro - numNodos;
        }

        public override string ToString()
        {
            return string.Format("C:{0:0.0000} S:{1:0.00} V:{2:0.00} F:{3:0.00}", TaxaCompra, TaxaStop, TaxaVenda, comportamento.ToString());
        }

        public void Resetar()
        {
            Pontuacao = 0;
            carteira = new Carteira();
        }



        public Investidor Clone()
        {
            return new Investidor(this.comportamento, this.TaxaStop, this.TaxaVenda, this.TaxaCompra);
        }

        double modificaValor(double original, double maximo)
        {
            if (rnd.Next(10) == 0)
                return rnd.NextDouble() * maximo;
            double v;
            if (rnd.Next(2) == 0)
            {
                v = original + 0.001;
                if (v > maximo)
                    v = maximo;
            }
            else
            {
                v = original - 0.001;
                if (v < 0)
                    v = 0;
            }

            return v;
        }

        double modificaTaxaCompra(double original)
        {
            double v;
            if (rnd.Next(2) == 0)
                v = original + 0.001;
            else
                v = original - 0.001;
            if (v > GeradorExpressoes.taxaMaximaCompra)
                v = GeradorExpressoes.taxaMaximaCompra;
            if (v < -GeradorExpressoes.taxaMaximaCompra)
                v = -GeradorExpressoes.taxaMaximaCompra;
            return v;

        }
        public Investidor Mutation()
        {
            switch (rnd.Next(4))
            {
                case 0:
                    return new Investidor(Expressao.ModificaExpressao(this.comportamento), this.TaxaStop, this.TaxaVenda, this.TaxaCompra);
                case 1:
                    return new Investidor(this.comportamento, modificaValor(this.TaxaStop, GeradorExpressoes.taxaMaximaStop), this.TaxaVenda, this.TaxaCompra);
                case 2:
                    return new Investidor(this.comportamento, this.TaxaStop, modificaValor(this.TaxaVenda, GeradorExpressoes.taxaMaximaVenda), this.TaxaCompra);
                case 3:
                default:
                    return new Investidor(this.comportamento, this.TaxaStop, this.TaxaVenda, modificaTaxaCompra(this.TaxaCompra));
            }
        }

        public Investidor CrossOver(Investidor B)
        {
            Expression<Func<IAvaliadorExpressao, Decisao>> exp;
            int chance = rnd.Next(0, 100);
            if (chance < 25)
                exp = this.comportamento;
            else if (chance < 50)
                exp = B.comportamento;
            else
                exp = Expressao.CrossOverUmPonto(this.comportamento, B.comportamento);

            double tstop = rnd.Next(0, 2) == 0 ? this.TaxaStop : B.TaxaStop;
            double tcompra = rnd.Next(0, 2) == 0 ? this.TaxaCompra : B.TaxaCompra;
            double tvenda = rnd.Next(0, 2) == 0 ? this.TaxaVenda : B.TaxaVenda;
            return new Investidor(
                exp, tstop, tvenda, tcompra);
        }


        public static Investidor CriaInvestidor()
        {
            var exp = Expressao.CriaExpressao();
            double tstop = rnd.NextDouble() * GeradorExpressoes.taxaMaximaStop;
            double tcompra = rnd.NextDouble() * GeradorExpressoes.taxaMaximaCompra * 2 - GeradorExpressoes.taxaMaximaCompra;
            double tvenda = rnd.NextDouble() * GeradorExpressoes.taxaMaximaVenda;
            return new Investidor(
                exp, tstop, tvenda, tcompra);
        }

        static Simulacao sim = new Simulacao(Acao.LerHistoricosPasta("c:\\fabio\\bolsa\\dados"));

        double? _fitness = null;

        double calcfitness()
        {
            if (contaNodos() > 4)
                return 0;

            double minimo = int.MaxValue;
            int bonus = 0;
            for (DateTime dtini = new DateTime(2002, 1, 1); dtini < new DateTime(2002, 12, 1); dtini = dtini.AddMonths(1))
            //for (DateTime dtini = new DateTime(2002, 1, 1); dtini < new DateTime(2006, 12, 1); dtini = dtini.AddYears(1))
            {
                //  DateTime dtfim = dtini.AddYears(1);
                var dtfim = dtini.AddMonths(1);
                sim.Simular(this, dtini, dtfim);
                this.CalculaPontuacao();
                double pontos = Pontuacao;// / divisor;
                if (pontos < minimo)
                    minimo = pontos;
                if (pontos >= 5000)
                    bonus++;
            }
            if (minimo >= 5000)
                return minimo - Math.Pow(2, contaNodos());
            return bonus - contaNodos() / 1000.0;
        }


        public double Fitness
        {
            get {
                if (_fitness.HasValue)
                    return _fitness.Value;
                _fitness = calcfitness();
                return _fitness.Value;
            }
        }

        public string Validar()
        {
            List<double> pontuacao = new List<double>();
            for (int i = 2009; i <= 2013; i++)
            {
                DateTime dtini = new DateTime(i, 1, 1);
                DateTime dtfim = dtini.AddYears(1);
                sim.Simular(this, dtini, dtfim);
                CalculaPontuacao();
                pontuacao.Add(this.Pontuacao);
            }
            return string.Join("; ", pontuacao.Select(x => x.ToString("0.00")));
        }

    }
}
