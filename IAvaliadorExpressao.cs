﻿using System;
namespace bolsa
{
    interface IAvaliadorExpressao
    {
        //bool AmplitudeMaiorQuePerc([Faixa(15,90,15)] int dias, [Faixa(10, 90, 10)] int perc);
        bool AmplitudeMenorQuePerc([Faixa(15, 90, 15)] int dias, [Faixa(10, 90, 10)] int perc);

        bool MenosQueXOscilacoes([Faixa(15, 90, 15)] int dias, [Faixa(10, 40, 10)] int varperc, [Faixa(1, 10, 1)] int x);
        //bool MaiorQueXOscilacoes([Faixa(15, 90, 15)] int dias, [Faixa(10, 40, 10)] int varperc, [Faixa(1, 10, 1)] int x);

        bool MenorQuePercentil([Faixa(10, 90, 10)] int perc, [Faixa(15, 90, 15)] int dias);
        //bool MaiorQuePercentil([Faixa(10, 90, 10)] int perc, [Faixa(15, 90, 15)] int dias);

        bool QuedaMaiorQue([Faixa(1, 50, 1)] int deciperc);
        bool AltaMaiorQue([Faixa(1, 50, 1)] int deciperc);

        bool RendimentoMenorQue([Faixa(-30, 30, 1)] int perc);

        bool RendimentoMaiorQue([Faixa(-30, 30, 1)] int perc);

        bool DiasCompradoMenorQue([Faixa(1, 120, 1)] int dias);
        bool DiasCompradoMaiorQue([Faixa(1, 120, 1)] int dias);

        bool MMEXMenorMedia([Faixa(15, 90, 15)]int diasMMEX, [Faixa(15, 90, 15)]int diasMedia);

    }
}
