﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{

    class DadosParametro
    {
        public int min;
        public int max;
        public int passo;
    }

    class DadosMetodo
    {
        public MethodInfo metodo;
        public DadosParametro[] dadosparametros;
    }
    class Metodos
    {

        static DadosMetodo[] _metodos;
        public static DadosMetodo[] LeMetodos()
        {
            if (_metodos == null)
            {
                _metodos = typeof(IAvaliadorExpressao).GetMethods().Where(x => x.ReturnType == typeof(bool)
                    && x.IsPublic
                    && x.GetParameters()
                        .All(y => y.ParameterType == typeof(int)
                            && y.GetCustomAttributes(typeof(FaixaAttribute), false).Count() == 1)
                    && !x.IsSpecialName)
                .Select(mi => new DadosMetodo()
                {
                    metodo = mi,
                    dadosparametros = mi.GetParameters().Select(mp => new DadosParametro()
                    {
                        min = ((FaixaAttribute)mp.GetCustomAttributes(typeof(FaixaAttribute)).Single()).Min,
                        max = ((FaixaAttribute)mp.GetCustomAttributes(typeof(FaixaAttribute)).Single()).Max,
                        passo = ((FaixaAttribute)mp.GetCustomAttributes(typeof(FaixaAttribute)).Single()).Passo
                    }
                ).ToArray()
                }).ToArray();
            }
            return _metodos;
        }


    }
}
