﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    interface ISolucao<T>
        where T : ISolucao<T>
    {
        T Clone();
        T Mutation();
        T CrossOver(T B);
        double Fitness { get; }

    }
}
