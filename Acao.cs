﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace bolsa
{
    public class Acao
    {
        public string ticker;
        public Cotacao[] historico;
        Dictionary<DateTime, PosicaoAcao> Indice;

        public override int GetHashCode()
        {
            return ticker.GetHashCode();
        }

        public PosicaoAcao PosicaoData(DateTime data)
        {
            PosicaoAcao p;
            if (Indice.TryGetValue(data, out p))
                return p;
            else
                return null;
        }

        public void Indexa()
        {
            Indice = new Dictionary<DateTime, PosicaoAcao>();
            for (int i = 0; i < historico.Length; i++)
                Indice[historico[i].Data] = new PosicaoAcao(i, this);
        }

        public bool ValidoPara(DateTime data)
        {
            int contador = 0;
            for (DateTime d = data; d < data.AddMonths(1); d = d.AddDays(1))
            {
                if (Indice.ContainsKey(d))
                    contador++;
            }
            return contador > 15;
        }

        public static Acao[] LerHistoricosPasta(string pasta)
        {
            List<Acao> lista = new List<Acao>();
            var arquivos = Directory.GetFiles(pasta).Where(x => x.EndsWith(".csv"));
            foreach (var arquivo in arquivos)
            {
                var acao = new Acao();
                acao.ticker = Path.GetFileNameWithoutExtension(arquivo);
                acao.historico = Cotacao.LerCSV(arquivo);
                if (acao.historico.Length < 100)
                    continue;
                lista.Add(acao);
                acao.Indexa();
            }
            return lista.Where(x => x.ValidoPara(new DateTime(2002, 1,1))).ToArray();
        }
    }
}
