﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    class Carteira
    {
        const double custoOperacao = 20;

        public class Item
        {
            public DateTime DataCompra;
            public Acao acao;
            public int Quotas;
            public double UltimoPreco;
            public double PrecoMedio;
        }

        public class OrdemCompra
        {
            public double preco;
            public double evitarGastarMaisQue;
        }

        public Dictionary<Acao, Item> Acoes;
        public Dictionary<Acao, OrdemCompra> OrdensCompra;
        public HashSet<Acao> OrdensVenda;
        public double Dinheiro;
        public double MinimaDinheiro = 0;

        public List<Historico> historico = new List<Historico>();

        public class Historico
        {
            public DateTime data { get; set; }
            public string Ticker { get; set; }
            public bool venda { get; set; }
            public int quotas { get; set; }
            public double preco { get; set; }
            public double valor
            {
                get
                {
                    return preco * quotas;
                }
            }

            public double variacao { get; set; }

            public override string ToString()
            {
                string operacao = venda ? "venda" : "compra";
                return string.Format("{4:dd-MM-yyyy} {0} de {1} quotas de {2} por {3:0.00} - var {5:0.00}", operacao, quotas, Ticker, valor, data, variacao);
            }

        }

        public Carteira()
        {
            this.Dinheiro = 5000;
            this.Acoes = new Dictionary<Acao,Item>();
            this.OrdensCompra = new Dictionary<Acao, OrdemCompra>();
            this.OrdensVenda = new HashSet<Acao>();
        }

        protected void VenderAcao(Acao acao, double preco, DateTime data)
        {
            Item item;
            if (!Acoes.TryGetValue(acao, out item))
                return;
            Dinheiro -= custoOperacao;
            Dinheiro += preco * item.Quotas;
            Acoes.Remove(acao);
#if DEBUG
            historico.Add(new Historico()
            {
                data = data,
                preco = preco,
                quotas = item.Quotas,
                Ticker = acao.ticker,
                variacao = item.Quotas * preco - item.PrecoMedio * item.Quotas,
                venda = true
            });
#endif
        }

        public void ExecutarOrdens(IEnumerable<PosicaoAcao> posicoes, double taxaStop, double taxaCompra, double taxaVenda, bool modoTeste)
        {
            if (Dinheiro < MinimaDinheiro)
                MinimaDinheiro = Dinheiro;
            foreach (var posicao in posicoes)
            {
                Item item;
                var acao = posicao.acao;
                var precoAtualizado = posicao.ValorAtual();

                if (Acoes.TryGetValue(acao, out item))
                {
                    item.UltimoPreco = precoAtualizado;

                    double precoStop = item.PrecoMedio - item.PrecoMedio * taxaStop;
                    double precoVenda = item.PrecoMedio + item.PrecoMedio * taxaVenda;

                    if (posicao.Cotacao.Low <= precoStop)
                    {
                        VenderAcao(acao, precoStop, posicao.Data);
                    }
                    else if (posicao.Cotacao.High > precoVenda)
                    {
                        VenderAcao(acao, precoVenda, posicao.Data);
                    }
                    else if (posicao.ExisteAnomalia || OrdensVenda.Contains(acao))
                    {
                        VenderAcao(acao, posicao.Cotacao.Close, posicao.Data);
                    }
                }
            }

            OrdensVenda.Clear();

            if (OrdensCompra.Count > 3)
            {
                OrdensCompra = OrdensCompra.Take(3).ToDictionary(x => x.Key, x => x.Value);
            }

            foreach (var posicao in posicoes)
            {
                var acao = posicao.acao;
                OrdemCompra ordem;
                if (OrdensCompra.TryGetValue(acao, out ordem))
                {
                    var precoCompra = ordem.preco;
                    precoCompra = precoCompra + precoCompra * taxaCompra;
                    if (posicao.Cotacao.Low <= precoCompra)
                    {
                        if (posicao.Cotacao.Open < precoCompra)
                            precoCompra = posicao.Cotacao.Open;
                        ComprarAcao(acao, precoCompra, ordem.evitarGastarMaisQue, posicao.Data, modoTeste);
                    }
                    else
                    {
                        //this.Dinheiro -= 2; 
                        // considerando custo para operação que não se concretiza
                        // porque a bovespa não deve permitir atolar de solicitações de compra que
                        // somando dariam mais do que temos de $$$
                    }
                }
            }

            OrdensCompra.Clear();
        }

        public void InserirOrdemCompra(Acao acao, double preco, double evitarGastarMaisQue)
        {
            OrdemCompra ordem = new OrdemCompra();
            ordem.evitarGastarMaisQue = evitarGastarMaisQue;
            ordem.preco = preco;
            OrdensCompra[acao] = ordem;
        }

        public void InserirOrdemVenda(Acao acao)
        {
            OrdensVenda.Add(acao);
        }

        public double ValorTotal()
        {
            double total = Dinheiro + Acoes.Sum(x => x.Value.Quotas * x.Value.UltimoPreco - custoOperacao);
            return total;
        }

        protected void ComprarAcao(Acao acao, double preco, double evitarGastarMaisQue, DateTime data, bool modoTeste)
        {
            if (modoTeste && evitarGastarMaisQue > (Dinheiro - custoOperacao))
                evitarGastarMaisQue = (Dinheiro - custoOperacao);

            int quotas = (int)(evitarGastarMaisQue / preco);
            if (quotas < 1)
                return;

            Item item;
            if (Acoes.TryGetValue(acao, out item))
            {
                item.PrecoMedio = ((item.Quotas * item.PrecoMedio) + (quotas * preco)) / (item.Quotas + quotas);
                item.Quotas += quotas;
                item.DataCompra = data;
            }
            else
            {
                item = new Item();
                item.acao = acao;
                item.Quotas = quotas;
                item.PrecoMedio = preco;
                item.DataCompra = data;
                Acoes[acao] = item;
            }
            this.Dinheiro -= quotas * preco;
            Dinheiro -= custoOperacao;
            item.UltimoPreco = preco;
#if DEBUG
            historico.Add(new Historico()
            {
                data = data,
                preco = preco,
                quotas = item.Quotas,
                Ticker = acao.ticker,
                venda = false
            });
#endif
        }
    }
}
