﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace bolsa
{
    class GeradorExpressoes
    {
        public Investidor melhorResultado;
        public Investidor[] Populacao = new Investidor[] {};
        Simulacao sim;

        public static double taxaMaximaCompra = 0.05;
        public static double taxaMaximaVenda = 0.5;
        public static double taxaMaximaStop = 0.5;

        const int PopulacaoMaxima = 10;
        const int taxaMutacao = 90;
        const int taxaCrossOver = 5;
        const int numElite = 1;
        public static int profundidadeInicial = 1;

        Investidor sorteioIndividuo()
        {
            int maximo = Populacao.Length * Populacao.Length;
            int posicao = (int) Math.Sqrt(rnd.Next(maximo));
            posicao = Populacao.Length - posicao;
            if (posicao < 0)
                posicao = 0;
            if (posicao >= Populacao.Length)
                posicao = Populacao.Length - 1;
            return Populacao[posicao];
        }

        public void Reseta()
        {
            var list = new List<Investidor>();
            for (int i = 0; i < PopulacaoMaxima; i++)
            {
                var expressao = Investidor.CriaInvestidor();
                list.Add(expressao);
            }
            Populacao = list.ToArray();
        }



        void ProcessaGeracao(DateTime dtIni, DateTime dtFim)
        {
            var novaGeracao = new List<Investidor>();
            novaGeracao.Add(Populacao.First().Clone());
            while (novaGeracao.Count() < PopulacaoMaxima)
            {
                var selecionado = sorteioIndividuo();
                var par = sorteioIndividuo();
                int chance = rnd.Next(100);
                if (chance < taxaMutacao)
                {
                    novaGeracao.Add(selecionado.Mutation());
                    novaGeracao.Add(par.Mutation());
                }
                else if (chance < (taxaMutacao + taxaCrossOver))
                {
                    novaGeracao.Add(selecionado.CrossOver(par));
                    novaGeracao.Add(par.CrossOver(selecionado));
                    //novaGeracao.Add(CrossOverUniforme(selecionado, par));
                    //novaGeracao.Add(CrossOverUniforme(selecionado, par));
                }
                else
                {
                    novaGeracao.Add(selecionado.Clone());
                    novaGeracao.Add(par.Clone());
                }
            }

            sim.Simular(novaGeracao, dtIni, dtFim);
            Populacao = novaGeracao
                .OrderByDescending(x => x.Fitness)
                .ToArray();
            melhorResultado = Populacao[0];
        }

        public GeradorExpressoes(Simulacao sim)
        {
        
            this.sim = sim;
            var inicial = Investidor.CriaInvestidor();
            Populacao = new Investidor[] { inicial };

        }

        public Investidor EncontraMelhorExpressao(int geracoes, DateTime dtIni, DateTime dtFim)
        {
            for (int i = 0; i < geracoes; i++)
                ProcessaGeracao(dtIni, dtFim);
            return melhorResultado;
        }

        void AdicionarNodosLista(Expression exp, List<ConditionalExpression> lista)
        {
            var cond = exp as ConditionalExpression;
            if (cond != null)
            {
                lista.Add(cond);
                AdicionarNodosLista(cond.IfTrue, lista);
                AdicionarNodosLista(cond.IfFalse, lista);
                return;
            }
        }






        
        public void Serializa(string nomeArq)
        {
            XDocument xml = new XDocument();
            XElement raiz = new XElement("populacao");
            foreach (var exp in Populacao)
            {
                raiz.Add(exp.serializar());
            }
            xml.Add(raiz);
            xml.Save(nomeArq);
        }

        public void EscreveDebug(string nomeArq)
        {
            var texto = Populacao
                .GroupBy(x => x.ToString())
                .Select(g => new
                {
                    count = g.Count(),
                    str = g.Key.ToString()
                })
                .OrderByDescending(x => x.count)
                .Select(x => string.Format("{0} x {1}", x.count, x.str));
            File.WriteAllLines(nomeArq, texto);
        }

        public Investidor MaisComum()
        {
            var texto = Populacao
                .GroupBy(x => x.ToString())
                .OrderByDescending(g => g.Count())
                .First();
            if (texto.Count() == 1)
                return melhorResultado;
            else
                return texto.First();
        }

        public void SerializaMaisComum(string nomeArq)
        {
            XDocument xml = new XDocument();
            xml.Add(MaisComum().serializar());
            xml.Save(nomeArq);
        }

        public void Deserializa(string nomeArq)
        {
            if (File.Exists(nomeArq))
            {
                var xml = XDocument.Load(nomeArq);
                var raiz = xml.Root;
                var exps = raiz.Elements().Select(x => Investidor.Deserializar(x)).ToArray();
                Populacao = exps;
            }
        }

        static Random rnd = new Random();












    }
}
