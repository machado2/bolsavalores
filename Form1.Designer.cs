﻿namespace bolsa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProcessaLoop = new System.Windows.Forms.Button();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnProcessaLoop
            // 
            this.btnProcessaLoop.Location = new System.Drawing.Point(220, 8);
            this.btnProcessaLoop.Name = "btnProcessaLoop";
            this.btnProcessaLoop.Size = new System.Drawing.Size(165, 24);
            this.btnProcessaLoop.TabIndex = 6;
            this.btnProcessaLoop.Text = "Processa Loop";
            this.btnProcessaLoop.UseVisualStyleBackColor = true;
            this.btnProcessaLoop.Click += new System.EventHandler(this.btnProcessaLoop_Click);
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(12, 39);
            this.txtResultado.Multiline = true;
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.Size = new System.Drawing.Size(673, 357);
            this.txtResultado.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(697, 408);
            this.Controls.Add(this.txtResultado);
            this.Controls.Add(this.btnProcessaLoop);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnProcessaLoop;
        private System.Windows.Forms.TextBox txtResultado;
    }
}

