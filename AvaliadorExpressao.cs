﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bolsa
{
    class AvaliadorExpressao : IAvaliadorExpressao
    {
        public PosicaoAcao Posicao;
        public Investidor Investidor;

        public AvaliadorExpressao(PosicaoAcao pos, Investidor invest)
        {
            this.Posicao = pos;
            this.Investidor = invest;
        }

        public bool AmplitudeMaiorQuePerc(int dias, int perc)
        {
            return Posicao.AmplitudeMaiorQuePerc(dias, perc);
        }

        public bool AmplitudeMenorQuePerc(int dias, int perc)
        {
            return Posicao.AmplitudeMenorQuePerc(dias, perc);
        }

        public bool MenosQueXOscilacoes(int dias, int varperc, int x)
        {
            return Posicao.MenosQueXOscilacoes(dias, varperc, x);
        }

        public bool MaiorQueXOscilacoes(int dias, int varperc, int x)
        {
            return Posicao.MaiorQueXOscilacoes(dias, varperc, x);
        }

        public bool MenorQuePercentil(int perc, int dias)
        {
            return Posicao.MenorQuePercentil(perc, dias);
        }

        public bool MaiorQuePercentil(int perc, int dias)
        {
            return Posicao.MaiorQuePercentil(perc, dias);
        }

        int PercRendimento()
        {
            double? custoMedio = Investidor.CustoMedio(this.Posicao.acao);
            if (custoMedio.HasValue && custoMedio > 0)
            {
                double valorAtual = this.Posicao.ValorAtual();
                double variacao = valorAtual - custoMedio.Value;
                return (int)((variacao / custoMedio) * 100);
            }
            else
                return 0;
        }

        public bool RendimentoMenorQue(int perc)
        {
            return PercRendimento() < perc;
        }

        public bool RendimentoMaiorQue(int perc)
        {
            return PercRendimento() > perc;
        }

        public bool ExisteAnomalia
        {
            get
            {
                return Posicao.ExisteAnomalia;
            }
        }


        public bool DiasCompradoMenorQue(int dias)
        {
            int? diasComprado = Investidor.DiasDesdeCompra(Posicao.acao, Posicao.Data);
            if (diasComprado.HasValue)
                return diasComprado.Value < dias;
            else
                return false;
        }

        public bool DiasCompradoMaiorQue(int dias)
        {
            int? diasComprado = Investidor.DiasDesdeCompra(Posicao.acao, Posicao.Data);
            if (diasComprado.HasValue)
                return diasComprado.Value > dias;
            else
                return false;
        }

        public bool MMEXMenorMedia(int diasMMEX, int diasMedia)
        {
            double mmex = Posicao.MMEX(diasMMEX);
            double media = Posicao.Media(diasMedia);
            return mmex < media;
        }


        public bool QuedaMaiorQue(int deciperc)
        {
            return Posicao.TaxaVariacao < deciperc / -1000;
        }

        public bool AltaMaiorQue(int deciperc)
        {
            return Posicao.TaxaVariacao < deciperc / 1000;
        }
    }
}
