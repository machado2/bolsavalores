﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bolsa
{
    public partial class FormSugestao : Form
    {
        public FormSugestao()
        {
            InitializeComponent();
        }

        string pastaBase = "c:\\fabio\\bolsa";

        string strdecisao(Decisao decisao)
        {
            if (decisao == Decisao.Esperar) return string.Empty;
            return decisao.ToString();
        }

        private void btnSugestao_Click(object sender, EventArgs e)
        {
            string caminhoCotacoes = Path.Combine(pastaBase, DateTime.Today.ToString("yyyyMMdd"));
            int t = 0;
            while (t < 7 && !Directory.Exists(caminhoCotacoes)) {
                caminhoCotacoes = Path.Combine(pastaBase, DateTime.Today.AddDays(-t).ToString("yyyyMMdd"));
                t++;
            }
            if (t >= 7)
                return;

            var acoes = Acao.LerHistoricosPasta(caminhoCotacoes);
            var investidor = Serializacao.DeserializarMelhor("c:\\fabio\\bolsa\\melhor.xml");
            var posicoes = acoes.Select(acao => new PosicaoAcao(acao.historico.Count() - 1, acao)).ToArray();

            var decisoes = posicoes.Select(pos => new
            {
                pos.acao.ticker,
                decisao = investidor.calculaDecisao(pos),
                preco = pos.acao.historico.Last().Close
            }).Select(x => new
            {
                x.ticker,
                comprar = x.decisao == Decisao.Comprar ? "Comprar" : string.Empty,
                vender = x.decisao == Decisao.Vender ? "Vender" : string.Empty,
                preco = (x.preco + x.preco * investidor.TaxaCompra).ToString("0.00")
            })         
            .ToArray();

            dataGridView1.DataSource = decisoes;
        }

        private void FormSugestao_Load(object sender, EventArgs e)
        {

        }

        Form1 frm;

        private void button1_Click(object sender, EventArgs e)
        {
            if (frm == null)
                frm = new Form1();
            frm.Show();
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            string caminhoCotacoes = Path.Combine(pastaBase, DateTime.Today.ToString("yyyyMMdd"));
            if (!Directory.Exists(caminhoCotacoes))
            {
                Directory.CreateDirectory(caminhoCotacoes);
            }
            btnDownload.Enabled = false;
            Task.Factory.StartNew((Action)delegate
            {
                for (int i = 0; i < DownloadYahoo.tickers.Length; i++)
                {
                    var ticker = DownloadYahoo.tickers[i];
                    string titulo = string.Format("{0}/{1} {2}", i+1, DownloadYahoo.tickers.Length, ticker);
                    if (this.IsDisposed)
                        break;
                    this.Invoke((Action)delegate
                    {
                        this.Text = titulo;
                    });
                    DownloadYahoo.DownloadTicker(caminhoCotacoes, ticker);
                }
                if (!this.IsDisposed)
                    this.Invoke((Action)delegate
                    {
                        this.Text = "OK";
                        btnDownload.Enabled = true;
                    });
            });
        }
    }
}
